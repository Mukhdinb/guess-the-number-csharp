﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuesTheNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            getAppInfo();

            greetUser();
            while (true)
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Random random = new Random();

                int number = random.Next(1, 10);


                int guess = 0;

                Console.WriteLine("Guess The Number between 1 and 10");
                Console.ResetColor();
                while (guess != number)
                {
                    string input = Console.ReadLine();
                    if (!int.TryParse(input, out guess))
                    {
                        printColorMessage(ConsoleColor.Red, "I said Number!");
                        continue;

                    }
                    guess = Int32.Parse(input);



                    if (guess == number)
                    {
                        printColorMessage(ConsoleColor.Green, "Correct! Good Job =)");
                    }
                    else
                    {
                        printColorMessage(ConsoleColor.Red, "Wrong! Try harder!");
                    }


                }
                Console.WriteLine("Play Again [Y or N]");
                string answer = Console.ReadLine().ToUpper();
                if(answer == "Y")
                {
                    continue;
                }else if(answer == "N")
                {
                    return;
                }
                else
                {
                    return;
                }

                
            }

        }

        static void getAppInfo()
        {
            string app_name = "Gues The Number";
            string version = "1.0.0.0";
            string author = "Mukhdin Bekhoev";

            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("{0}: version:{1} by {2}", app_name, version, author);

            Console.ResetColor();

        }
        
        static void greetUser()
        {
            Console.WriteLine("Who are you?");
            string inputN = Console.ReadLine();
            Console.WriteLine("Hello {0}, lets play", inputN);

        }

        static void printColorMessage(ConsoleColor color, string msg)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(msg);
            Console.ResetColor();
        }
    }
}
